import { Padaria } from "src/padaria/entities/padaria.entity"
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm"

@Entity()
export class Categoriaz {

    @PrimaryGeneratedColumn('increment')
    id: number

    @Column()
    nome: string

    @Column()
    ano: number

    @Column()
    categoria: string

    @ManyToOne(() => Padaria, padaria => padaria.categoria) // nao sei pq está dando esse erro.. 
    padaria: Padaria;

    @Column()
    curso: string

    @CreateDateColumn()
    createdAt: string

    @UpdateDateColumn()
    updatedAt: string
}
