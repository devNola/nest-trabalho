import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CategoriasService } from './categorias.service';
import { CreateCategoriasDto } from './dto/create-categorias.dto';
import { UpdateCategoriasDto } from './dto/update-categorias.dto';

@Controller('categorias')
export class CategoriasController {
  constructor(private readonly CategoriasService: CategoriasService) { }

  @Post()
  create(@Body() createCategoriasDto: CreateCategoriasDto) {
    return this.CategoriasService.create(createCategoriasDto);
  }

  @Get()
  findAll() {
    return this.CategoriasService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.CategoriasService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCategoriasDto: UpdateCategoriasDto) {
    return this.CategoriasService.update(+id, updateCategoriasDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.CategoriasService.remove(+id);
  }
}
