import { Categoriaz } from 'src/categorias/entities/categoriaz.entity';
import { Module } from '@nestjs/common';
import { CategoriasService } from './categorias.service';
import { CategoriasController } from './categorias.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Padaria } from 'src/padaria/entities/padaria.entity';


@Module({
  imports: [TypeOrmModule.forFeature([Categoriaz, Padaria])],
  controllers: [CategoriasController],
  providers: [CategoriasService],
})
export class TurmaModule { }
