import { ArrayUnique, IsArray, IsInt, IsNumber, IsOptional, IsString } from "class-validator"

export class CreateCategoriasDto {

    @IsString()
    nome: string

    @IsNumber()
    ano: number

    @IsString()
    categoria: string

    @IsNumber()
    @IsOptional()
    padariaId?: number

}
