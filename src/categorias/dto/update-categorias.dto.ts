import { PartialType } from '@nestjs/mapped-types';
import { CreateCategoriasDto } from './create-categorias.dto';

export class UpdateCategoriasDto extends PartialType(CreateCategoriasDto) { }
