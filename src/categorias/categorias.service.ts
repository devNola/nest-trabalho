import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCategoriasDto } from './dto/create-categorias.dto';
import { UpdateCategoriasDto } from './dto/update-categorias.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Categoriaz } from './entities/categoriaz.entity';
import { In, Repository } from 'typeorm';
import { Padaria } from 'src/padaria/entities/padaria.entity';

@Injectable()
export class CategoriasService {

  constructor(
    @InjectRepository(Categoriaz)
    private categoriasRepository: Repository<Categoriaz>,
    @InjectRepository(Padaria)
    private padariaRepository: Repository<Padaria>
  ) { }

  async create(createCategoriasDto: CreateCategoriasDto) {
    const categorias = this.categoriasRepository.create(createCategoriasDto);

    let padaria = null;
    if (createCategoriasDto.padariaId) {
      padaria = await this.padariaRepository.findOneBy({ id: createCategoriasDto.padariaId })
      if (!padaria) {
        throw new Error('Produto não encontrado')
      }
      categorias.padaria = padaria
    }

    return this.categoriasRepository.save(categorias);
  }

  findAll() {
    return this.categoriasRepository.find({ relations: ['padaria'] });
  }

  async findOne(id: number) {
    const categorias = await this.categoriasRepository.findOne({
      where: { id },
      relations: ['padaria']
    })
    if (!categorias) {
      throw new NotFoundException(`Categoria com ID ${id} não encontrada`);
    }
    return categorias;
  }

  async update(id: number, updateCategoriasDto: UpdateCategoriasDto) {
    const categorias = await this.findOne(id);
    if (!categorias) {
      throw new NotFoundException(`Categorias com ID ${id} não foi encontrada`);
    }

    if (updateCategoriasDto.padariaId) {
      const padaria = await this.padariaRepository.findOneBy({ id: updateCategoriasDto.padariaId })
      if (!padaria) {
        throw new Error('Produto não encontrado')
      }
      categorias.padaria = padaria;
    }

    this.categoriasRepository.merge(categorias, updateCategoriasDto);
    return this.categoriasRepository.save(categorias);
  }

  async remove(id: number) {
    const result = await this.categoriasRepository.delete(id)
    if (result.affected === 0) {
      throw new NotFoundException(`Categorias com ID ${id} não encontrada`)
    }
    return result;
  }
}
