import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PadariaService } from './padaria.service';
import { CreatePadariaDto } from './dto/create-padaria.dto';
import { UpdatePadariaDto } from './dto/update-padaria.dto';

@Controller('padarias')
export class PadariaController {
  constructor(private readonly padariaService: PadariaService) { }

  @Post()
  create(@Body() createPadariaDto: CreatePadariaDto) {
    return this.padariaService.create(createPadariaDto);
  }

  @Get()
  findAll() {
    return this.padariaService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.padariaService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePadariaDto: UpdatePadariaDto) {
    return this.padariaService.update(+id, updatePadariaDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.padariaService.remove(+id);
  }
}
