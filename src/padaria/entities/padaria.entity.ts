import { Categoriaz } from "src/categorias/entities/categoriaz.entity";
import { Usuario } from "src/usuario/entities/usuario.entity"; // não iremos mexer
import { Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Padaria {

    @PrimaryGeneratedColumn('increment')
    id: number

    @OneToOne(() => Usuario)
    @JoinColumn()
    usuario: Usuario

    @OneToMany(() => Categoriaz, categorias => categorias.padaria)
    turmas: Categoriaz[];
}
