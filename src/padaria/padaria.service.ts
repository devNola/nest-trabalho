import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePadariaDto } from './dto/create-padaria.dto';
import { UpdatePadariaDto } from './dto/update-padaria.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Padaria } from './entities/padaria.entity';
import { Repository } from 'typeorm';
import { Usuario } from 'src/usuario/entities/usuario.entity';

@Injectable()
export class PadariaService {

  constructor(
    @InjectRepository(Padaria)
    private padariaRepository: Repository<Padaria>,
    @InjectRepository(Usuario)
    private usuarioRepository: Repository<Usuario>
  ) { }

  async create(createPadariaDto: CreatePadariaDto) {
    const usuario = await this.usuarioRepository.findOneBy({ id: createPadariaDto.usuarioId })
    if (!usuario) {
      throw new Error('Usuário não encontrado');
    }

    const padaria = this.padariaRepository.create({
      usuario
    })

    return this.padariaRepository.save(padaria);
  }

  findAll() {
    return this.padariaRepository.find({
      relations: ['usuario', 'turmas']
    });
  }

  async findOne(id: number) {
    const padaria = await this.padariaRepository.findOne({
      where: { id },
      relations: ['usuario', 'categorias']
    })

    if (!padaria) {
      throw new NotFoundException(`Produto com ID ${id} não encontrado`);
    }

    return padaria;
  }

  async update(id: number, updatePadariaDto: UpdatePadariaDto) {
    const padaria = await this.findOne(id);
    if (updatePadariaDto.usuarioId) {
      const usuario = await this.usuarioRepository.findOneBy({ id: updatePadariaDto.usuarioId })
      if (!usuario) {
        throw new Error('Usuário não encontrado');
      }
      padaria.usuario = usuario;
    }
    return this.padariaRepository.save(padaria);
  }

  async remove(id: number) {
    const result = await this.padariaRepository.delete(id)
    if (result.affected === 0) {
      throw new NotFoundException(`Professor com ID ${id} não encontrado`)
    }
    return result;
  }
}
