import { Module } from '@nestjs/common';
import { PadariaService } from './padaria.service';
import { PadariaController } from './padaria.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Padaria } from './entities/padaria.entity';
import { Usuario } from 'src/usuario/entities/usuario.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Padaria, Usuario])],
  controllers: [PadariaController],
  providers: [PadariaService],
})
export class PadariaModule { }
