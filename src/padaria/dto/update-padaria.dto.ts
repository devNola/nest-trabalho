import { PartialType } from '@nestjs/mapped-types';
import { CreatePadariaDto } from './create-padaria.dto';

export class UpdatePadariaDto extends PartialType(CreatePadariaDto) { }
